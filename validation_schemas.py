user_schema = {
    'type': 'object',
    'properties': {
        'user_id': {"type": ["integer", "string"],
                    "pattern": "^\d+$"}
    },
    'required': ['user_id'],
    "additionalProperties": False
}

book_add_schema = {
    'type': 'object',
    'properties': {
        'book_name': {"type": "string"},
        'book_author': {"type": "string"},
        'amount': {"type": ["integer", "string"],
                   "pattern": "^\d+$"}
    },
    'required': ['book_name', 'book_author', 'amount'],
    "additionalProperties": False
}

user_take_or_return_book_schema = {
    'type': 'object',
    'properties': {
        'user_id': {"type": ["integer", "string"],
                    "pattern": "^\d+$"},
        'book_id': {"type": ["integer", "string"],
                    "pattern": "^\d+$"},
    },
    'required': ['user_id', 'book_id'],
    "additionalProperties": False
}
