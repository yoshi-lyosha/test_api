### Перевод

[Русский](/ru)--[Английский](/)

# Описание

Test API для новых кандидатов для отдела тестирования API

## Инструменты

- Python 3.6+
- pytest
- requests

## О тестовом API

Это простой API для обычной библиотеки. Есть две сущности - Книги
и Пользователи. Пользователи могут хранить только 3 разных книги
одновременно. У Книги в библиотеке есть количество. Книга с
количеством меньше 1 - не может быть взята из библиотеки Пользователем

## Адрес веб-сайта в сети Интернет

http://yoshilyosha.pythonanywhere.com/api/v1/

## Методы API

Все методы API должны поддерживать GET и POST

Параметры могут передаваться с помощью url query или путем передачи
данных с помощью data и хедера Content-Type application/json

Парсинг параметров: сначала url query, затем полученные данные сливаются
с данными из json (если есть)

- book - получить книгу по id
    - "book_id" - int, id запрошенной книги
- book/add - добавить новую книгу
    - "book_name" - str, название новой книги
    - "book_author" - str, автор новой книги
    - "amount" - int, количество
- books - получить список всех книг
- user - получить пользователя по id
    - "user_id" - int, идентификатор запрашиваемого пользователя
- user/add - добавить нового пользователя
    - "user_name" - str, имя нового пользователя
- users - получить список всех пользователей
- user/take_book - предоставить книгу «book_id» пользователю «user_id»
    - "user_id" - int
    - "book_id" - int
- user/return_book - вернуть книгу «book_id» от пользователя
«user_id»
    - "user_id" - int
    - "book_id" - int
- test/repopulate_db - специальный метод API тестирования для
повторного заполнения db
    - "amount" - int, default = 5, количество автоматически генерируемых
данных

# Некоторые подсказки

- Используйте PyCharm и исправьте все предупреждения, которые показывает
анализ кода (пожалуйста)
- Прочтите PEP-8 не менее одного раза
- Параметризуйте запросы HTTP-методов (GET / POST) с помощью
@pytest.mark.parametrize (вы можете параметризовать весь класс)
- Параметризуйте методы отправки данных (query / json) с помощью
@pytest.mark.parametrize (вы можете параметризовать весь класс)
- Не забывайте о заголовке Content-Type для json-данных (или вы можете
просто использовать аргумент `json` в либе requests)
- Используйте pytest.skip или pytest.xfail для зарепорченных баг:
```
response = request.post ('url', params = {"something": "invalid"})
if response.status_code == 500:
    pytest.skip ('bug number 420 - bad validation in api/endpoint')
```
Вы можете написать аналогичные скипы не только для status_codes,
но и для пропуска других ошибок, такие как неподдерживаемый http_method
или неподдерживаемый тип передачи данных (query/json). Больше
интересного - здесь:
[pytest skipping](https://docs.pytest.org/en/latest/skipping.html)
- **Проверить поля "body" и "status" ответа JSON, а не только
http status_code:**
`response.json() == template_dict`
- Сохраните свою работу в репозитории на github/gitlab/bitbucket/ в
любом месте, избегайте отправки архива с кодом
- Добавьте в свой репозиторий requirements.txt и хорошо оформленный
README.md
- Обогатите свой код докстрингами и комментариями. Опишите в каждом
тесте что он делает
- Старайтесь не писать одинаковые случаи для аналогичных методов API:
напишите единый тест и параметризуйте его методами API
- Попробуйте подготовить тест-кейс уникальными данными, например -
создайте новую книгу и сохраните ее id для проверки методов book/ или
books/
- Постарайтесь хорошо организовать свои тесты. Вы можете создать дерево
файлов для каждого из них
    - ./api/v1/tests/book/add_test.py,
    - ./api/v1/tests/book/test.py
    - ...
- Сделайте ваши тесты атомарными. Один тест - одна проверка одного
метода API и одного действия. Не пытайтесь проверять все в одном
тесте. Если у вас одинаковые действия для других методов API -
параметризуйте это


# Что читать

- [Статья на habrahabr] (https://habr.com/post/269759/)
- [Документация pytest] (https://docs.pytest.org/en/latest/)