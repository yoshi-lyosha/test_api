# Score table of applicant's homework solution

### How to use:

Just count the value of each estimation

- `(0 : n)` - score from range \[0, 1, …, n\]
- `(0 | n)` - 0 or n score points

------

### Estimation of testing theory

- `[ ] (0 : 16)` API bugs
    - `[ ] (0 : 6)` no validation on endpoints
        - `[ ] (0 | 1)` book
        - `[ ] (0 | 1)` books (additionalProperties)
        - `[ ] (0 | 1)` user/add
        - `[ ] (0 | 1)` users (additionalProperties)
        - `[ ] (0 | 1)` user/return_book
        - `[ ] (0 | 1)` test/repopulate_db
    - `[ ] (0 | 2 | 4)` only GET endpoints
        - `[ ] (0 | 2)` users
        - `[ ] (0 | 2)` book/add
    - `[ ] (0 | 3)` only query
        - user/take_book
    - `[ ] (0 | 3)` only json
        - user/return_book

- `[ ] (0 : 12)` Business logic bugs
    - `[ ] (0 | 4)` no check on book giving to user (book could be given
     even if its amount less than 1)
    - `[ ] (0 | 4)` no check on books storing by user (user can take any
     positive amount of books)
    - `[ ] (0 | 4)` no actions with book amount (it's doesn't change,
    rofl)

- `[ ] (0 : 5)` Not a bags, but features reported
    - `[ ] (0 | 1)` adding the same user
    - `[ ] (0 | 1)` adding the same book
    - `[ ] (0 | 1)` picking the same book
    - `[ ] (0 | 1)` necessity of ISBN for book identification
    - `[ ] (0 | 1)` necessity of Passport data for user identification
    - `[ ] (-1)` remark about necessity in prod of method that
    drops db :} (also about confusion when one tester drops db while
    another tester's tests are running)

- `[ ] (0 : 18)` Testing
    - `[ ] (0 : 6)` endpoints coverage
        - `[ ] (0 | 1)` few endpoints are covered
        - `[ ] (0 | 1)` all endpoints are covered
        - `[ ] (0 : 2)` GET/POST http methods are covered
            - `[ ] (0 | 1)` partially
            - `[ ] (0 | 1)` full coverage
        - `[ ] (0 : 2)` query/json
            - `[ ] (0 | 1)` partially
            - `[ ] (0 | 1)` full coverage
    - `[ ] (0 : 8)` cases coverage
        - `[ ] (0 : 2)` functional
            - `[ ] (0 | 1)` partially
            - `[ ] (0 | 1)` full coverage
        - `[ ] (0 : 2)` wrong params
            - `[ ] (0 | 1)` partially
            - `[ ] (0 | 1)` full coverage
        - `[ ] (0 : 4)` interesting ang surprising cases
    - `[ ] (-2 | 1 : 4)` well-designed bugs description
        - `-2` no bug reports
        - `1` at least some bug report
        - `2` attempt to design it well
        - `3` good bug report for every bug
        (what was done|what was awaited|what was gotten)
        - `4` excellent bug report
    - `[ ] (?)` some advices for a developer about improvements

### Estimation of coding skills

- `[ ] (0 : 6)` Misc.
    - `[ ] (0 | 2)` only english in code/repo
    - `[ ] (0 | 2)` repo has a README.md
    - `[ ] (-2 | 2)` requirements.txt
    - `[0 : n]` interesting features and solutions (1 : 3 score points
    for each one, depending on its efficiency impact)
    - `[-n : 0]` code bugs, bad coding, etc. (-3 : -1) score points
    for each one, depending on its efficiency impact)
    - etc.

- `[ ] (0 : 12)` Code
    - `[ ] (-3 : 3)` nice variables names
    - `[ ] (-3 : 3)` atomic functions
    - `[ ] (-3 : 3)` code reuse
    - `[ ] (-3 : 3)` comments/docstrings

- `[ ] (0 : 12)` Python
    - `[ ] (-3 : 3)` PEP 8
    - `[ ] (-3 : 3)` pythonic-way of programming
    - `[ ] (0 : 3)` usage of python features (genexp, listcomp, etc.)
    - `[ ] (0 : 3)` custom decorators

- `[ ] (0 : 46)` Pytest and autotests
    - `[ ] (-1 | 1 : 4)` tests code storing organization
        - `-1` bad organization, only first-lvl nested functions in one
        module
        - `1 - 2` attempts to organize code well (f.ex. tests are divided
        by long comment strings
        - `3 - 4` good code organization (1 module for each endpoint,
        etc.)
    - `[ ] (-2 | 1 : 4)` common_api_actions module creation and usage
        - `-1` no reuse of common api actions
        - `[ ] (0 : 2)`  actions are partitioned in atomic functions
            - `[ ] (0 | 1)` some
            - `[ ] (0 | 1)` a lot
        - `[ ] (0 : 2)` api response checking
    - `[ ] (-1 | 2)` isolation of each test-case (avoiding test-cases
    like 'first test-case - created the book, next test-case - uses this
    created book')
    - `[ ] ( -1 | 1 - 6)` wrong params tests
        - `-1` no wrong params tests at all
        - `1` duplication of tests, but covering some of endpoints
        - `2` duplication of tests, but covering almost all endpoints
        - `4` some parametrization and some special tools for this
        purpose
        - `6` opus magnum of wrong params testing
    - `[ ] (0 | 2 - 4)` parametrization on http methods
    - `[ ] (0 | 2 - 4)` parametrization on data sending (query/json)
    - `[ ] (0 : 3)` skips on bugs
    - `[ ] (0 : 3)` well designed fixtures (or setup/teardown)
    - `[ ] (-1 | 1 - 9)` checks of API response
        - `-1` no checks
        - `[ ] (0 | 1)` checking of response's http status
        - `[ ] (0 | 2)` checking json response
        - `[ ] (0 | 2)` checking of response's headers
        - `[ ] (0 - 4)` some ultimate solution of API response check (checks
        http status, json structure, a lot of headers, etc.)
    - `[ ] (0 : 3)` nice assertions prints (maybe some custom
    solutions/site-packages
    - `[ ] (0 - 4)` taking care about an external influence
        - `0` any external influence can fail the tests
        - `4` each test has personal setup


## Results Table

```
========================= Testing =========================

          + -------- + ---------- + -------- + ------- +
          | API bugs | Logic bugs | Features | Testing |
          | ======== | ========== | ======== | ======= |
applicant |          |            |          |         |
          | -------- | ---------- | -------- | ------- |
   max    |    16+   |     12+    |    5+    |   18+   |
          + -------- + ---------- + -------- + ------- +


========================= Coding ==========================

          + -------- + ---------- + -------- + ------- +
          |   Misc   |   Coding   |  Python  |  Pytest |
          | ======== | ========== | ======== | ======= |
applicant |          |            |          |         |
          | -------- | ---------- | -------- | ------- |
   max    |    6+    |     12+    |    12+   |   46+   |
          + -------- + ---------- + -------- + ------- +

```
