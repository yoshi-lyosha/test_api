### Translation

[Russian](/ru)--[English](/)

# Description

Test API for new applicants for API testing department

## Tools to use

- Python 3.6+
- pytest
- requests

## About test API

It's a simple API for a regular Library. There are two entities - Books
and Users. Users can hold only 3 different books simultaneously. Each
book has amount of its in the Library. Book that amount is 1 - can't be
taken by a User

## Location

http://yoshilyosha.pythonanywhere.com/api/v1/

## Endpoints

All endpoints must support GET and POST http methods

Parameters can be passed by url query or by passing the data
with Content-Type application/json header or both

Parameters parsing rule: query at first, then updates by json

- book - get book by id
    - "book_id" - int, id of requested book
- book/add - add new book
    - "book_name" - str, name of the new book
    - "book_author" - str, author of the new book
    - "amount" - int, amount of the new book
- books - get list of all books
- user - get user by id
    - "user_id" - int, id of requested user
- user/add - add new user
    - "user_name" - str, name of the new user
- users - get list of all users
- user/take_book - give book by "book_id" to user by "user_id"
    - "user_id" - int
    - "book_id" - int
- user/return_book - return book by "book_id" from user by "user_id"
    - "user_id" - int
    - "book_id" - int
- test/repopulate_db - special test endpoint for repopulating db
    - "amount" - int, default = 5, amount of auto generated data

# Some hints

- Use PyCharm and fix all warnings that code analysis shows (please)
- Read PEP-8 at least once
- Parametrize requests HTTP methods (GET/POST) by
@pytest.mark.parametrize (you can parametrize the whole class)
- Parametrize data sending methods (query/json) by
@pytest.mark.parametrize (you can parametrize the whole class)
- Don't forget about Content-Type header for json data (or you can just
use `json` argument in requests lib)
- Use pytest.skip or pytest.xfail for reported bugs:
```
response = requests.post('url', params={"something": "invalid"})
if response.status_code == 500:
    pytest.skip('bug number 420 - bad validation in api/endpoint')
```
[pytest skipping](https://docs.pytest.org/en/latest/skipping.html)
You can write the similar skips not only for status_codes, but also for
skipping another bugs like unsupported http_method or data transition
type
- **Check JSON response body and status, not only status_code**
- Save your work in repo on github/gitlab/bitbucket/anywhere, avoid
sending the archives with your code
- Enrich your repo with requirements.txt and well-designed README.md
- Enrich your code with docstrings and comments. Tell in every test
case, what this case is doing
- Try to avoid writing the same cases for similar endpoints: write a
single test-case and parametrize it by endpoints
- Try to setup the test-case with the unique data, e.g. - create a new
book and save its id for testing the book/ or books/ endpoint
- Try to organize well your tests. You can create a tree of files for
each endpoint like
    - ./api/v1/tests/book/add_test.py,
    - ./api/v1/tests/book/test.py
    - …
- Atomize your tests. One test - one endpoint and one sigle action
check. Don't try to check everything in one single test. If you have the
same action for another endpoints - parametrize it


# What to read

- [Article on habrahabr](https://habr.com/post/269759/)
- [pytest doc](https://docs.pytest.org/en/latest/)
