from collections import defaultdict

import markdown
import jsonschema
from flask import Flask, Blueprint, jsonify, request, make_response, Markup

import validation_schemas
from library import LibraryWithBugs
from library_db import LibraryDBbyJSON

app = Flask(__name__)
db = LibraryDBbyJSON()
bug_lib = LibraryWithBugs(db)

bug_lib_api_prefix = '/api/v1'
bug_lib_api = Blueprint('storage_api', __name__, url_prefix=bug_lib_api_prefix)

"""
Bugs list:
- no check on adding the same user
- no check on adding the same book
- no validation on parameters
- no limit for book picking (literally no, you can take infinite amount of books)
- no limit for book giving (book can be given when its amount less or equal than 0)
- no actions with book amount (it's doesn't change, rofl)
- you can pick the same books
- users and book/add has only GET http method
- user/take_book uses only query params
- user/return_book uses only json params
- etc.
"""


def get_request_params(_request):
    params = _request.args.to_dict()

    request_json = _request.json
    if request_json:
        params.update(request_json)
    return params


def check_request_params(params, schema):
    error_body = defaultdict(list)

    v = jsonschema.Draft4Validator(schema)
    for error in v.iter_errors(params):
        # decoding is for un-escaping \\\\d to \\d for example
        error_msg = error.message.encode("utf-8").decode('unicode_escape')
        if len(error.path) > 0:
            error_body[error.path.pop()] = error_msg
        else:
            error_body['additional_errors'].append(error_msg)

    return error_body


@app.route('/')
def index():
    content = open('README.md').read()
    content = Markup(markdown.markdown(content))
    return content


@app.route('/ru')
def index_ru():
    content = open('README_RU.md').read()
    content = Markup(markdown.markdown(content))
    return content


@app.errorhandler(404)
def _handle_api_error(ex):
    if request.path.startswith(bug_lib_api_prefix):
        return make_response(jsonify({'status': 404, 'body': ''}), 404)
    else:
        return ex


@app.errorhandler(500)
def _handle_api_error(ex):
    if request.path.startswith(bug_lib_api_prefix):
        return make_response(jsonify({'status': 500, 'body': 'internal error'}), 500)
    else:
        return ex


@bug_lib_api.route('/user/add', methods=['GET', 'POST'])
def user_add():
    params = get_request_params(request)

    user_name = params.get('user_name')

    new_user = bug_lib.add_user(user_name)

    resp_json = {"status": 200, "body": {'id': new_user.user_id,
                                         'name': new_user.user_name,
                                         'taken_books_ids': new_user.books_list}}
    return make_response(jsonify(resp_json), 200)


@bug_lib_api.route('/user', methods=['GET', 'POST'])
def user():
    params = get_request_params(request)
    error_body = check_request_params(params, validation_schemas.user_schema)
    if error_body:
        resp_json = {'status': 400, 'body': error_body}
        return make_response(jsonify(resp_json), 400)

    user_id = int(params.get('user_id'))

    bug_lib_user = bug_lib.get_user(user_id)

    resp_json = {"status": 200,
                 "body": {'id': bug_lib_user.user_id,
                          'name': bug_lib_user.user_name,
                          'taken_books_ids': bug_lib_user.books_list}}
    return make_response(jsonify(resp_json), 200)


@bug_lib_api.route('/users', methods=['GET'])
def users():
    bug_lib_users = bug_lib.get_users_list()
    users_list = [{'id': _user.user_id, 'name': _user.user_name, 'taken_books_ids': _user.books_list}
                  for _user in bug_lib_users]
    resp_json = {'status': 200, 'body': {'users': users_list}}
    return make_response(jsonify(resp_json), 200)


@bug_lib_api.route('/book/add', methods=['GET'])
def book_add():
    params = get_request_params(request)
    error_body = check_request_params(params, validation_schemas.book_add_schema)
    if error_body:
        resp_json = {'status': 400, 'body': error_body}
        return make_response(jsonify(resp_json), 400)

    book_name = params.get('book_name')
    book_author = params.get('book_author')
    amount = int(params.get('amount'))

    new_book = bug_lib.add_book(book_name, book_author, amount)

    resp_json = {"status": 200, "body": {'id': new_book.book_id, 'name': new_book.book_name,
                                         'author': new_book.book_author, 'amount': new_book.amount}}
    return make_response(jsonify(resp_json), 200)


@bug_lib_api.route('/book', methods=['GET', 'POST'])
def book():
    params = get_request_params(request)

    book_id = int(params.get('book_id'))

    bug_lib_book = bug_lib.get_book(book_id)

    resp_json = {"status": 200,
                 "body": {'id': bug_lib_book.book_id, 'name': bug_lib_book.book_name,
                          'author': bug_lib_book.book_author, 'amount': bug_lib_book.amount}}
    return make_response(jsonify(resp_json), 200)


@bug_lib_api.route('/books', methods=['GET', 'POST'])
def books():
    bug_lib_books = bug_lib.get_books_list()
    books_list = [{'id': _book.book_id, 'name': _book.book_name,
                   'author': _book.book_author, 'amount': _book.amount}
                  for _book in bug_lib_books]
    resp_json = {'status': 200, 'body': {'books': books_list}}
    return make_response(jsonify(resp_json), 200)


@bug_lib_api.route('/user/take_book', methods=['GET', 'POST'])
def user_take_book():
    params = request.args.to_dict()
    error_body = check_request_params(params, validation_schemas.user_take_or_return_book_schema)
    if error_body:
        resp_json = {'status': 400, 'body': error_body}
        return make_response(jsonify(resp_json), 400)

    user_id = int(params.get('user_id'))
    book_id = int(params.get('book_id'))

    bug_lib.give_book_to_user(book_id, user_id)

    resp_json = {"status": 200, "body": ""}
    return make_response(jsonify(resp_json), 200)


@bug_lib_api.route('/user/return_book', methods=['GET', 'POST'])
def user_return_book():
    params = request.json

    user_id = int(params.get('user_id'))
    book_id = int(params.get('book_id'))

    bug_lib.return_book_from_user(book_id, user_id)

    resp_json = {"status": 200, "body": ""}
    return make_response(jsonify(resp_json), 200)


@bug_lib_api.route('/test/repopulate_db', methods=['GET', 'POST'])
def repopulate_db():
    params = get_request_params(request)
    amount = params.get('amount', 5)
    bug_lib.repopulate_db(amount)

    resp_json = {"status": 200, "body": ""}
    return make_response(jsonify(resp_json), 200)


app.register_blueprint(bug_lib_api)

if __name__ == '__main__':
    app.run()
