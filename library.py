from random import randint
from typing import List, Union

from faker import Faker


class LibraryUser:
    def __repr__(self):
        return f"{type(self).__name__}({self.user_name!r}" \
               f", user_id={self.user_id}" \
               f", books_list={self.books_list})"

    def __init__(self, user_name, books_list=None, user_id=None):
        self.user_id = user_id
        self.user_name = user_name
        if books_list is None:
            books_list = []
        self.books_list = books_list


class LibraryBook:
    def __repr__(self):
        return f"{type(self).__name__}({self.book_name!r}, {self.book_author}" \
               f", book_id={self.book_id}, amount={self.amount})"

    def __init__(self, book_name, book_author, amount, book_id=None):
        self.book_id = book_id
        self.book_name = book_name
        self.book_author = book_author
        self.amount = amount


class LibraryDB:
    def save_user(self, library_user: LibraryUser):
        raise NotImplementedError

    def save_book(self, library_book: LibraryBook):
        raise NotImplementedError

    def get_user(self, library_user_id) -> Union[LibraryUser, None]:
        raise NotImplementedError

    def get_book(self, library_book_id) -> Union[LibraryBook, None]:
        raise NotImplementedError

    def get_all_users(self) -> List[LibraryUser]:
        raise NotImplementedError

    def get_all_books(self) -> List[LibraryBook]:
        raise NotImplementedError

    def drop(self):
        raise NotImplementedError


class LibraryWithBugs:
    """
    Bugs list:
    - no check on adding the same user
    - no check on adding the same book
    """
    MAX_BOOKS = 3

    def __init__(self, db: LibraryDB):
        self.db = db

    def add_user(self, user_name):
        new_user = LibraryUser(user_name)
        self.db.save_user(new_user)
        return new_user

    def add_book(self, book_name, book_author, amount):
        # if not exist - create, if exists - increase amount
        new_book = LibraryBook(book_name, book_author, amount)
        self.db.save_book(new_book)
        return new_book

    def get_books_list(self) -> list:
        return self.db.get_all_books()

    def get_users_list(self) -> list:
        return self.db.get_all_users()

    def get_user(self, user_id) -> LibraryUser:
        return self.db.get_user(user_id)

    def get_book(self, book_id) -> LibraryBook:
        return self.db.get_book(book_id)

    def give_book_to_user(self, book_id, user_id):
        user = self.get_user(user_id)
        user.books_list.append(book_id)
        self.db.save_user(user)

    def return_book_from_user(self, book_id, user_id):
        user = self.get_user(user_id)
        user.books_list.remove(book_id)
        self.db.save_user(user)

    def repopulate_db(self, amount=5):
        self.db.drop()

        fake = Faker()
        for i in range(amount):
            user = LibraryUser(fake.name())
            self.db.save_user(user)
            book = LibraryBook(fake.sentence(nb_words=4).rstrip('.'), fake.name(), amount=randint(0, 100))
            self.db.save_book(book)
            # somehow create connections between


if __name__ == '__main__':
    from library_db import LibraryDBbyJSON
    lib = LibraryWithBugs(LibraryDBbyJSON())
    lib.repopulate_db()
