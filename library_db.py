import os
import json
from typing import Union, List

import peewee as pw

from library import LibraryDB, LibraryUser, LibraryBook


class LibraryDBbyJSON(LibraryDB):
    db_name = 'library_db.json'
    db_template = {'users': {},
                   'books': {}}
    db = {}

    def __init__(self):
        if not os.path.exists(self.db_name):
            with open(self.db_name, 'w') as db:
                json.dump(self.db_template, db)

        self._load_db()

    def drop(self):
        with open(self.db_name, 'w') as db:
            json.dump(self.db_template, db)

    def _load_db(self):
        with open(self.db_name) as db:
            self.db = json.load(db)
            self.db['users'] = {int(k): v for k, v in self.db['users'].items()}
            self.db['books'] = {int(k): v for k, v in self.db['books'].items()}

    def _save_db(self):
        with open(self.db_name, 'w') as db:
            json.dump(self.db, db)

    def save_user(self, library_user: LibraryUser):
        self._load_db()
        if not self.db['users'].get(library_user.user_id):
            library_user.user_id = 0 if len(self.db['users']) == 0 else max(self.db['users']) + 1
        self.db['users'][library_user.user_id] = {'user_name': library_user.user_name,
                                                  'books_list': library_user.books_list}
        self._save_db()

    def save_book(self, library_book: LibraryBook):
        self._load_db()
        if not self.db['books'].get(library_book.book_id):
            library_book.book_id = 0 if len(self.db['books']) == 0 else max(self.db['books']) + 1
        self.db['books'][library_book.book_id] = {'book_name': library_book.book_name,
                                                  'book_author': library_book.book_author,
                                                  'amount': library_book.amount}
        self._save_db()

    def get_user(self, library_user_id) -> Union[LibraryUser, None]:
        self._load_db()
        db_user = self.db['users'].get(library_user_id)
        if db_user:
            user = LibraryUser(user_id=library_user_id,
                               user_name=db_user['user_name'],
                               books_list=db_user['books_list'])
            return user
        return None

    def get_book(self, library_book_id) -> Union[LibraryBook, None]:
        self._load_db()
        db_book = self.db['books'].get(library_book_id)
        if db_book:
            book = LibraryBook(book_id=library_book_id,
                               book_name=db_book['book_name'],
                               book_author=db_book['book_author'],
                               amount=db_book['amount'])
            return book
        return None

    def get_all_users(self) -> List[LibraryUser]:
        self._load_db()
        users = []
        for library_user_id in self.db['users']:
            user = self.get_user(library_user_id)
            users.append(user)
        return users

    def get_all_books(self) -> List[LibraryBook]:
        self._load_db()
        books = []
        for library_book_id in self.db['books']:
            book = self.get_book(library_book_id)
            books.append(book)
        return books


class LibraryDBbyORM(LibraryDB):
    # TODO: finish that class
    db_name = 'library_db.db'

    def __init__(self):
        self.db = pw.SqliteDatabase(self.db_name)

        class User(pw.Model):
            id = pw.IntegerField(primary_key=True)
            user_name = pw.CharField()

            class Meta:
                database = self.db

        self.User = User

        class Book(pw.Model):
            id = pw.IntegerField(primary_key=True)
            book_name = pw.CharField()
            amount = pw.IntegerField(default=0)

            class Meta:
                database = self.db

        self.Book = Book

        class TakenBooks(pw.Model):
            book_taker = pw.ForeignKeyField(self.User, backref='books_taken')
            book_taken = pw.ForeignKeyField(self.Book, backref='users_taken')

            class Meta:
                database = self.db

        self.TakenBooks = TakenBooks

        self.db.create_tables([User, Book, TakenBooks])

    def save_user(self, library_user: LibraryUser):
        query = self.User.select().where(self.User.id == 11)
        if not query.exist:
            self.User.create()

    def save_book(self, library_book: LibraryBook):
        pass

    def get_user(self, library_user_id) -> LibraryUser:
        pass

    def get_book(self, library_book_id) -> LibraryBook:
        pass

    def get_all_users(self) -> List[LibraryUser]:
        pass

    def get_all_books(self) -> List[LibraryBook]:
        pass

    def drop(self):
        pass
